package com.nuax.s3manager.service;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ManagerService {

    File convertMultiPartToFile(MultipartFile file) throws IOException;

    String uploadFileToS3bucket(String fileName, File file);

    byte[] downloadFileFromS3bucket(String filename);

    String deleteFileFromS3bucket(String filename);

    List<S3ObjectSummary> getFromS3bucketList();


}
