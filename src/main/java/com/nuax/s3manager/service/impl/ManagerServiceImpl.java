package com.nuax.s3manager.service.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.nuax.s3manager.service.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ManagerServiceImpl implements ManagerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagerServiceImpl.class);


    @Autowired
    private AmazonS3 s3client;

    @Value("${endpointUrl}")
    private String endpointUrl;

    @Value("${bucketName}")
    private String bucketName;

    @Override
    public File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    @Override
    public String uploadFileToS3bucket(String fileName, File file) {
        try {
            s3client.putObject(new PutObjectRequest(bucketName, fileName, file));
        }catch(AmazonServiceException e) {
            return "Uploading failed :" + e.getMessage();
        }
        return "Uploading Successfull";
    }

    @Override
    public byte[] downloadFileFromS3bucket(String fileName) {
        byte[] content = null;
        final S3Object s3Object = s3client.getObject(bucketName, fileName);
        final S3ObjectInputStream stream = s3Object.getObjectContent();
        try {
            content = IOUtils.toByteArray(stream);
            s3Object.close();
        } catch(IOException e) {
            LOGGER.info("IO Error Message= " + e.getMessage());
        }
        return content;
    }

    @Override
    public String deleteFileFromS3bucket(String filename) {
        try {
            s3client.deleteObject(filename, bucketName);
        }catch(AmazonServiceException e) {
            return "delete failed :" + e.getMessage();
        }
        return "Delete Successfull";
    }

    @Override
    public List<S3ObjectSummary> getFromS3bucketList() {
        ObjectListing objectListing = s3client.listObjects(new ListObjectsRequest().withBucketName(bucketName));
        List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();
        return s3ObjectSummaries;
    }
}
