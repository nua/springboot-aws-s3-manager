package com.nuax.s3manager.controller;

import com.nuax.s3manager.service.ManagerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;


@RestController
@RequestMapping("/home/")
public class ManagerController {

    @Resource
    private ManagerService managerService;

    @Value("${resourceUrl}")
    private String resourceUrl;


    @PostMapping("/uploadFile")
    public String uploadFile(@RequestPart(value = "file") MultipartFile multipartFile) {

        String fileUrl = "";
        String status = null;
        try {
            File file = managerService.convertMultiPartToFile(multipartFile);
            String fileName = multipartFile.getOriginalFilename();
            fileUrl = resourceUrl + fileName;
            status = managerService.uploadFileToS3bucket(fileName, file);
            file.delete();
        } catch (Exception e) {
            return e.getMessage();
        }
        return status + " ==> " + fileUrl;
    }

    @PostMapping("/downloadFile")
    public ResponseEntity<ByteArrayResource> downloadFile(@RequestParam(value = "fileName") String filename) {
        final byte[] data = managerService.downloadFileFromS3bucket(filename);
        final ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + filename + "\"")
                .body(resource);
    }

}
