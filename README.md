# Springboot-AWS-S3-manager

본 Application은 Spring-boot와 연동하여 AWS S3 client를 이용한 fileupload, filedelete, filedownload, filelist등의 S3 file을 manage 할 수있는 기능을 구현합니다. 

## S3 client를 이용하여 file upload, download, delete  

파일업로드는 8080 port로 구되게 설정되어있으며, file upload시에 해당 파일은 application.properties에 설정된 bucketName=bespin-nuax에 저장되게 됩니다.
해당 파일업로드를 하기 위해선 AWS의 accessKey와 secretKey키를 생성하여 설정 하여야 한다.

파일을 업로드하기 위해서는 JAVA용 라이브러리인 aws-java-sdk가 필요합니다.
현재기준 aws-java-sdk의 최신버전은 1.11.804입니다.
```
<dependency>
   <groupId>com.amazonaws</groupId>
   <artifactId>aws-java-sdk</artifactId>
   <version>1.11.804</version>
</dependency>
```
파일을 업로드하기 위해 Amazone의 Client 설정을 다음과 같이 설정 하여야 합니다.
 
```java
@Bean
public AmazonS3 s3client() {
    
    BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
    AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                            .withRegion(Regions.fromName(region))
                            .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                            .build();
    
    return s3Client;
}
```
 
s3Client를 이용하여 bucketName, fileName, file을 매개변수로 전달하여 파일을 업로드 합니다.
 
```java
s3client.putObject(new PutObjectRequest(bucketName, fileName, file));
```

파일이 정상적으로 업로드 되면 AWS console의 S3 버킷에서 파일확인.

## 파일 Manage 예제 HTML
파일업로드를 위해 index.html을 HTML과 Bootstrap을 이용하여 심플하게 구성.  

## 참고사항
실제 배포 서버인 EC2에서의 S3연동을 하기 위해서는 IAM 설정을 통하여 할당된 IAM ROLE에 따라 accessKey와 secretKey를 발급 받아 사용 가능합니다.

## 진행중
추후 이미지 및 file등의 암호화 적용 예정